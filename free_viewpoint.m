function imgOut = free_viewpoint(image1, image2, p)
% This function generates an image from a virtual viewpoint between two
% real images. The output image has the same size as the input images.

%% Initalize Vairables 

print = true;
[iy,ix,iz] = size(image1);


%% Rectification

[image1, image2, stereoImage] = rectification_fitted(image1, image2, 1);

%--Zum Einzeichen von horizontalen Linien im image
%
if print
    figure; imshow(stereoImage);
    hold on;
    for ii = 1:50:size(stereoImage,1)
    line([1 size(stereoImage,2)],[ii ii],'Color','red'); 
    end 
    hold off;
end 
%
%-- 


%% Disparity Map 

image1 = img_resize(image1,0.5);
image2 = img_resize(image2,0.5);

% %--- R1L1--------- (Image orig size)
%   mins     = -530;
%   maxs     = 150;
%   win_size = 30;
%   weight   = 6;

%--- R2L2--------- (Image orig size)
% mins     = -400;
% maxs     = 200;
% win_size = 30;
% weight   = 5;

%--- R1L1--------- (Image size 0.5)
%mins     = -270;
%maxs     = 80;
%win_size = 15;
%weight   = 6;

%--- R2L2--------- (Image size 0.5)
mins     = -200;
maxs     = 100;
win_size = 20;    %15
weight   = 100;   %5

[disparityMap_1, disparityMap_2] = disparityEstimation(image1, image2, mins, maxs, win_size, weight);

if print
    figure;imshow([disparityMap_1, disparityMap_2],[mins,maxs]);
end 


%% View Synthesis 

%1.Schritt:
[imgOut1, refEdges] = genInitalView(p, image1, image2, disparityMap_1, disparityMap_2);

%2.Schritt:
imgOut2 = genRefinementView(refEdges, imgOut1, 5);

%3.Schritt:
[imgOut3,holesEdges] = fillImage(imgOut2, 15, 6);

%4.Schritt:
imgOut4 = genRefinementView(holesEdges, imgOut3, 5);

%Ausgabe der einzelnen Schritte:
if print
    figure; imshow(imgOut1);figure;imshow(refEdges); 
    figure; imshow(imgOut2);
    figure;imshow(imgOut3);
    figure;imshow(holesEdges);figure;imshow(imgOut4);
end 


%% Aufloesung anpassen:

imgOut = defaultresolution(imgOut4,ix,iy);


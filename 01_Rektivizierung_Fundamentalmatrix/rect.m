function [irect1mk,irect2mk,grids,corners] = rect(im_left,im_right,offset_komp)
%RECT Rektiviziert zwei Bilder, macht sie anschlie�end gleich gro�, und
%falls offset_komp true ist wird eines der Bilder verschoben so, dass 
%die Merkmalspunkte in den gleich Zeilen liegen (in etwa).
% [irect1,g1x,g1y,H1,irect2,g2x,g2y,H2] = rect(img1,img2)
% img1:                 Linkes Bild (Farbild oder Graubild)
% img2:                 Rechtes Bild (Farbild oder Graubidl)
% g1x,g1y:              X - und -Y Grid von linkem, rektivizierten Bild
% g2x,g2y:              X - und -Y Grid von rechtem, rektivizierten Bild
% H1,H2:                Transformationsmatrizen mit denen die beiden 
%                       Bilder rektiviziert wurden
% irect1:               Rektiviziertes linkes Bild
% irect2:               Rektiviziertes rechtes Bild
% offset_komp:          Wenn 1 dann wird kompensiert (noch zeitintensiv)
% grids:                Struktur mit g1x,g1y,g2x,g2y
% corners:              Struktur mit corner.left (linkes Bild) und 
%                       corners.right (rechtes Bild)

%% Graubild Konvertierung
    img1_g                          = rgb2gray(im_left);
    img2_g                          = rgb2gray(im_right);
    
%% Berechnung der Fundamentalmatrix
    visualize                       = 0;
    [F,mP1,mP2]                     = get_F(img1_g,img2_g,visualize);
    
%% Berechung von der Transformationsmatrizen H1 und H2
    [H1,H2]                         = get_H1H2(F,mP1,mP2,size(im_left));
    
%% Transformtion der Bildkoordinaten (Rektifizierung)
    [im_r1,g1x,g1y]                 = warp_img(im_left,H1);
    [im_r2,g2x,g2y]                 = warp_img(im_right,H2); 

%% Pr�fung der Rektivizierung (normal aus)
    proof               = 0;
    if proof
        visualize_matches_rect(im_r1,g1x,g1y,im_r2,g2x,g2y,...
                                                        mP1,mP2,H1,H2);
    end
  
%% Eckpunkte berechnen
    % Linkes Bild
    [corn_l]                        = calc_rect_corners(...
                                              size(im_left),H1,g1x,g1y);
    % Rechtes Bild
    [corn_r]                        = calc_rect_corners(...
                                             size(im_right),H2,g2x,g2y);
    % Testen
    test_corners                    = 0;
    if test_corners
        visualize_matches_one_img(corn_l,im_r1);
        visualize_matches_one_img(corn_r,im_r2);
    end
 
%% Match sizes
    [irect1m,irect2m,c_change_m]    = match_rect_sizes(im_r1,im_r2);
    corn_l_rm                       = shift_coords(corn_l,...
                                       c_change_m(1,2),c_change_m(1,1));
    corn_r_rm                       = shift_coords(corn_r,...
                                       c_change_m(2,2),c_change_m(2,1)); 

%% Offsetkompensierung
    if offset_komp
    % Merkmalspunktkoordinaten im rektivizierten Bild
    mP1_rect                        = calc_rect_coords(g1x,g1y,mP1,H1);
    mP2_rect                        = calc_rect_coords(g2x,g2y,mP2,H2);
    % Merkmalspunktkoordinaten f�r die matched sizes
    mP1_rm                          = shift_coords(mP1_rect,...
                                       c_change_m(1,2),c_change_m(1,1));
    mP2_rm                          = shift_coords(mP2_rect,...
                                       c_change_m(2,2),c_change_m(2,1));
    % Offset f�r machted sizes
    offset_m                        = get_rect_offset(mP1_rm,mP2_rm);
    
    % Offset kompensieren
    [irect1mk,irect2mk,c_change_k]  = comp_offset(...
                                              irect1m,irect2m,offset_m);
    % Punkte in den kompensierten, rektivizierten gematchten
    % Bildern
    test                            = 0;
    if test
    mP1_rmk                         = shift_coords(mP1_rm,...
                                       c_change_k(1,2),c_change_k(1,1));
    visualize_matches_one_img(mP1_rmk,irect1mk);
    mP2_rmk                         = shift_coords(mP2_rm,...
                                       c_change_k(2,2),c_change_k(2,1));
    visualize_matches_one_img(mP1_rmk,irect1mk);                               
    end
    corn_l_rmk                      = shift_coords(corn_l_rm,...
                                       c_change_k(1,2),c_change_k(1,1));
    corn_r_rmk                      = shift_coords(corn_r_rm,...
                                       c_change_k(2,2),c_change_k(2,1));                               
    else
        % Falls nicht kompensiert wird einfach die nicht kompensierten
        % Variablen verwenden
        % Merkmalspunkte werden dann nicht gebraucht
        irect1mk                    = irect1m;
        irect2mk                    = irect2m;
        corn_l_rmk                  = corn_l_rm;
        corn_r_rmk                  = corn_r_rm;
    end
    
    % Ausgabe der Grids
    grids.g1x                       = g1x;
    grids.g1y                       = g1y;
    grids.g2x                       = g2x;
    grids.g2y                       = g2y;
    
    % Ausgabe der Eckpunkte
    corners.left                    = corn_l_rmk;
    corners.right                   = corn_r_rmk;
    
end    

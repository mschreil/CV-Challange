function [H1,H2] = get_H1H2(F,mP1,mP2,dim)
%GET_H1H2 Berechnet die Transformationsmatrizen f�r die Rektifizierung
% [H1,H2] = get_H1H2(F,mP1,mP2,dim)
% mP1:          Unkalibrierte Koordinaten der Mateched Points von Bild 1
%               (im Consensius set enthalten, inliers -> robust) (2 x n)
% mP2:          Unkalibrierte Koordinaten der Mateched Points von Bild 2
%               (im Consensius set enthalten, inliers -> robust) (2 x n)
% F:            Fundamentalmatrix
% H1:           Transformationsmatrix f�r das linke Bild
% H2:           Transformatonsmatrix f�r das rechte Bild
% dim:          Vektor mit [xdim ydim] (1x2)

f = 1;

%% Berechnung von H2
    % Berechung Epipol e2' und e1' (in unkalibrierten Koordinaten -> 
    % Pixelkoordinaten (e2' liegt im Kern von F-transposed)
    [u,v,w]     = svd(F);
    e2          = u(:,3);
    e2n         = f/e2(3) * e2;  % Normierung der z-Koordinate auf 1 bzw f
    e1          = w(:,3);
    e1          = f/e1(3) * e1;   % Normierung der z-Koordinate auf 1 bzw f 
    
    % Berechnung der einzelenen Matrizen
    ydim        = dim(1);
    xdim        = dim(2);
    o_x         = xdim/2;
    o_y         = ydim/2;
    G_T         = [1 0 -o_x;...  % Verschiebt den Bildmittelpunkt in den 
                   0 1 -o_y;...  % Nullpunkt. Das bedeutet [0 0 1]' 
                   0 0  1];      % liegt jetzt im Bildmittelpunkt. Das
                                 % bedeutet, dass die Z-Achse senkrecht
                                 % in den Bildmittelpunkt zeigt 
                                 % -> Drehungen um die z-Achse drehen 
                                 % das Bild
    e2nt        = G_T * e2n;
    Phi         = atan(-e2nt(2)/e2nt(1));
    cp          = cos(Phi);
    sp          = sin(Phi);
    G_R         = [ cp -sp 0 ; sp cp 0 ; 0 0 1 ];
    e2ntr       = G_R * e2nt;    % Epipol muss rotiert werden
    x_e         = e2ntr(1);
    G           = [1 0 0 ; 0 1 0 ; -1/x_e 0 1];
    H2          = G * G_R * G_T;
    H2_mine     = H2;
    
%% Berechnung von H1 optimal f�r m�glichst wenig Verzerrung
    % H1 = H2*H
    % H ist nicht eindeutig (H = T'_dach * F + T' * v_transposed, wobei v
    % frei w�hlbar ist)
    % Es gilt x2_dach * H * x1 != 0 ->[ x2_dach * (H = T'_dach * F + 
    % T' * v_transposed) * x1 != 0 ] 
    % es entspricht 
    length      = size(mP1,2);
    % Umschreiben f�r Lesbarkeit
    x1          = mP1;
    x2          = mP2;
    T           = e2;
    A = [];   B = [];
    for i = 1:length
        x1_i    = [x1(:,i) ; f];
        x2_i    = [x2(:,i) ; f];
        % Koeffizientenmatrix
        Tx1_mat = kron(x1_i,T);
        A_temp  = hat(x2_i) * reshape(Tx1_mat,3,3);
        A       = [A ; A_temp];
        % Konstanten
        B_temp  = -hat(x2_i) * (hat(T))' * F * x1_i;
        B       = [B ; B_temp];
    end
    % Minimierung mit mldivide
    v           = A\B;
    % H bestimmen war das Ziel der Optimierung
    H           = (hat(T))' * F + T * v';
    % H1 bestimmen war Ziel insgesamt
    H1          = H2 * H;
    H1_mine     = H1;
end


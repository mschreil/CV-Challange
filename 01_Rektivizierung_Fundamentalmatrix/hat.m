function [x_hat] = hat(x)
%HAT Bildet Schiefsymetrische Matrix 
% [x_hat] = hat(x)
% x:        Eingabe:  Vektor der Form [a b c]'
% x_hat:    R�ckgabe: Schiefsymetrische Matrix der Form [ 0 -c  b]
%                                                       [ c  0 -a]
%                                                       [-b  a  0]

% Eingabe Argument                                                        
a = x(1);   b = x(2);   c = x(3);

% Ausgabe Argument
x_hat = [0 -c  b;...
         c  0 -a;...
        -b  a  0]; 
end


function [gx_gray,gy_gray] = get_warped_grid(grid_x,grid_y,H)
%GET_WARPED_GRID Berechnet das 'gewarpedte' Grid, das �ber das alte Bild
%gelegt wird um dann die alten Intensit�ten reinzuinterpolieren. Das 
%zur�ckgegebene Grid ist f�r ein Graubild ausreichend. F�r ein Farbild 
%muss es noch in der dritten Dimension erweitert werden (RGB). 
% [gx_gray,gy_gray] = get_warped_grid(grid_x,grid_y,H)
% grid_x:               Neues Grid, das durch Anwendung von H auf die 
%                       Eckpunkte des alten Bildes entstanden ist
% grid_y:               Neues Grid, das durch Anwendung von H auf die 
%                       Eckpunkte des alten Bildes entstanden ist
% H:                    Transformationsmatrix zum Warpen
% gx_gray:              X - Grid, das �ber das Bild zum Interpolieren 
%                       gelegt werden kann. F�r Graubild ausreichend,
%                       daher der Name .._gray.
% gy_gray:              Y - Grid, das �ber das Bild zum Interpolieren 
%                       gelegt werden kann. F�r Graubild ausreichend,
%                       daher der Name .._gray.

% Bildung aller normierten Koordinaten im Grid, das bei der 
% Transformtion ensteht, in Reihenform
    [y_dim, x_dim]      = size(grid_x);
    coords_n            = grid2lin(grid_x,grid_y);
    K_n                 = [coords_n;ones(1,size(coords_n,2))]; % normierte 
                                                             % Koordinaten
                                                       
% Berechnung der normierten Koordinaten W_n des gegeben Bildes, die 
% die Gleichung H * W_n = K_n am besten l�sen (Optimierungsproblem)
    W_n                 = normalize(H\K_n);  
    
% normierte Koordinaten W_n in ein Grid wandeln, das dann �ber dem
% alten Bild liegt
    [gx_gray,gy_gray]   = lin2grid(W_n,[y_dim,x_dim]);
    
end


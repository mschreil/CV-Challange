function [x_grid,y_grid] = lin2grid(koords_lin,grid_size)
%LIN2GRID berechnet aus dem Koordinaten - Vektor das Grid zur�ck.
%koords_lin muss eigentlich vorher aus grid entstanden sein und ist
%ansonsten nicht wirklich sinnvoll.
% [x_grid,y_grid] = lin2grid(koords_lin,grid_size)
% grid_size:            Gr��e der zur�ckgegebenen Grids (1 x 2)
%                       Bsp: [m n] 
% koords_lin:           Koordinaten - Vektor, der mit grid2lin erzeugt
%                       wurde (2 x m*n) oder normierte (3 x m*n)
% x_grid:               X - Koordinaten Meshgrid (m x n)
% y_grid:               Y - Koordinaten Meshgrid (m x n)

% Eingabe - �berpr�fung
    m_times_n       = grid_size(1) * grid_size(2);
    if m_times_n ~= size(koords_lin,2)
        error('My Error: Anzahl der Koordinaten stimmt nicht mit der geforderten Meshgrid-Gr��e �berein')
    end
    
    if eq(size(koords_lin,1),3)
        if koords_lin(3,:) ~= ones(1,size(koords_lin,2))
            warning('My Warning: Koordinaten sind nicht normiert')
        end
    end
    
% R�ckwandlung in grid bzw. Ausgabe
    x_grid          = reshape(koords_lin(1,:),grid_size(1),grid_size(2));
    y_grid          = reshape(koords_lin(2,:),grid_size(1),grid_size(2));

end


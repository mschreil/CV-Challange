function [w_img,grid_x,grid_y] = warp_img(img,H)
%WARP_IMG wendet Transformationsmatrix H (entweder H1 oder H2) auf das Bild
% [w_img,gxw,gyw] = warp_img(img,H)
% img (entweder img1 oder img2) an
% img:          Bild das gewarpt (rektifiziert) werden soll 
% H:            Transformationsmatrix mit der img gewarpt werden soll
% w_img:        gewarptes Bild, das zur�ckgegeben wird
% gxw           Meshgrid f�r X des neuen Bildes
% gyw           Meshgrid f�r Y des neuen Bildes

%% Neues Meshgrid f�r das gewarpte Bild
    size_img        = size(img);
    [grid_x,grid_y] = get_new_grid(size_img,H);
    
%% die neuen Intensit�tswerte zu finden
    % Berechung des warped Grids, das zur Intensit�tsberechnung �ber das
    % alte Bild gelegt wird
    [gxw,gyw]       = get_warped_grid(grid_x,grid_y,H);
    
    % F�r Farbbild
    gxw             = repmat(gxw,1,1,size(img,3));
    gyw             = repmat(gyw,1,1,size(img,3));
    
    % Neue Intensit�tswerte durch Interpolation
    w_img           = zeros(size(gxw));
    for i = 1:size(img,3)
        w_img(:,:,i)= interp2(double(img(:,:,i)),gxw(:,:,i),gyw(:,:,i),'bilinear');
    end
    
    
    


end


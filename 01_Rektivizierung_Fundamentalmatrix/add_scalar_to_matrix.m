function [scalar_added] = add_scalar_to_matrix(matrix,scalar)
%ADD_SCALAR_TO_VECTOR addiert ein Skalar auf einen Vektor (interne
%Funktion)
% [scalar_added] = add_scalar_to_vector(vector,scalar)
% matrix:           Matrix oder Vektor
% scalar:           Skalar der auf diesen Vektor addiert wird
% scalar_added:     R�ckgegebener Vektor auf den Skalar addiert wurde
    scalar_added        = matrix + scalar * ones(size(matrix));
end


function [offset] = get_rect_offset(mP1_rect,mP2_rect)
%GET_RECT_OFFSET berechnet den durchschnittlichen Y - Versatz zwischen
%den Merkmalspunkten im linken rektivizierten Bild und im rechten 
%rektivizierten Bild. Es gilt dann 
%koord_y_rect_right =(ca.) koord_y_rect_left - offset
% [offset] = get_rect_offset(mP1_rect,mP2_rect)
% mP1_rect:             Koordinaten der Merkmalspunkte im rektivizierten
%                       linken Bild (2 x n)
% mP2_rect:             Koordinaten der Merkmalspunkte im rektivizierten
%                       rechten Bild (2 x n)

% Berechnung des Y - Versatzes
    offset_y        = mP1_rect(2,:) - mP2_rect(2,:);
    offset_mean     = round(mean(offset_y));
    
% R�ckgabe
    offset          = offset_mean;
    
end


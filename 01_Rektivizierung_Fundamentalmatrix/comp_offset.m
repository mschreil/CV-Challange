function [irect1_k,irect2_k,c_change] = comp_offset(irect1,irect2,offset)
%COMPENSATE_OFFSET Kompensiert Offset durch hinzuf�gen von Nullzeilen,
%damit die Merkmalspunkte wieder in den selben Zeilen liegen.
%Funktioniert f�r matched size oder nicht matched size dadurch, dass 
%die Ausgabebilder immer gleich gro� gehalten werden. Dazu wird beim 
%nach unter zu verschiebenden Bild das Bild um 2 * abs(offset) nach 
%unten verschoben und das andere Bild oben und unten mit abs(offset)-
%Nullleisten erg�nzt/angef�gt f�r die gleiche Gr��e.
% [irect1_k,irect2_k,c_change] = compensate_offset(irect1,irect2,offset)
% [irect1_k,irect2_k] = compensate_offset(irect1,irect2,offset)
% irect1:           Rektiviziertes Bild links 
% irect2:           Rektiviziertes Bild rechts
% offset:           Offset berechnet durch get_rect_offset() 
% c_change:         Koordinatenver�nderung (ben�tigt falls Koordinaten
%                   im neuen Bild, das nurch die nan  in der Dimension 
%                   ver�ndert wurde, berechnet werden sollen) 
%                   [yl+ xl+;
%                    yr+ xr+] (Zahlen die auf alte Koordinaten addiert
%                   werden m�ssen) (yl: Zahl die auf linkes Bild addiert
%                   muss usw.)

% Offset ganzzahlig machen
    offset              = round(offset);

% Initialisierung
    c_change            = zeros(2,2);
    
% Dimensionen
    [~,xd1,zd1]         = size(irect1);
    [~,xd2,zd2]         = size(irect2);
    
% Fallunterscheidung um herausfinden, wie die Bilder verschoben werden
% m�ssen
if offset > 0
    % Rechtes Bild muss um abs(offset) nach unten verschoben werden
    % (weil Koordinaten des rechten Bild gr��er(Bildkoordinaten!) werden
    % m�ssen)
    irect2_komp         = [nan(2*abs(offset),xd2,zd2);irect2];
    % Ausgleich der Kompensierung des zweiten Bildes im ersten Bild
    irect1_komp         = [nan(abs(offset),xd1,zd1);...
                                      irect1;...
                           nan(abs(offset),xd1,zd1)];
    % Koordinatenver�nderung
    c_change(2,1)       = 2 * abs(offset);
    c_change(1,1)       = abs(offset);
else
    % Linkes Bild muss um offset nach unten verschoben werden
    irect1_komp         = [nan(2*abs(offset),xd1,zd1);irect1];
    % Ausgleich der Kompensierung des ersten Bildes im zweiten Bild
    irect2_komp         = [nan(abs(offset),xd2,zd2);...
                                      irect2;...
                           nan(abs(offset),xd2,zd2)];
    % Koordinatenver�nderung
    c_change(1,1)       = 2 * abs(offset);
    c_change(2,1)       = abs(offset);
end

% R�ckgabe
    irect1_k            = irect1_komp;
    irect2_k            = irect2_komp;
    
end


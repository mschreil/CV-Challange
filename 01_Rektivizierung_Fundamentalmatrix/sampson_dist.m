function sd = sampson_dist(F, x1_pixel, x2_pixel)
    % Diese Funktion berechnet die Sampson Distanz basierend auf der
    % Fundamentalmatrix F
    %e_3 Dach 
    e_dach=[0,-1,0; 1,0,0; 0,0,0];
    
    %Zaehler der Sampson-Distanz berechnen
    zaehler=sum((x2_pixel'*F)'.*x1_pixel,1).^2 ;
    
    %Nenner der Sampson-Distanz berechnen
    norm_1=e_dach*F*x1_pixel;
    norm_1=sum(norm_1.*norm_1,1);
    norm_2=x2_pixel'*F*e_dach;
    norm_2=sum(norm_2.*norm_2,2)';
    nenner=norm_1+norm_2;
    
    %Sampson-Distanz 
    sd=zaehler./nenner;
end


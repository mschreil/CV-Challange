function [c_out] = shift_coords(c_in,x_shift,y_shift)
%SHIFT_COORDS addiert x_shift auf die X - Koordinaten und y_shift auf 
%die Y - Koordinaten
% [c_out] = shift_coords(c_in,x_shift,y_shift)
% c_in:                 Koordinaten die zu verschieben sind
% x_shift:              Verschiebewert in X - Richtung
% y_shift:              Verschiebewert in Y - Richtung



% X - Richtung
    c_temp(1,:) = add_scalar_to_matrix(c_in(1,:),x_shift);
    
% Y - Richtung
    c_temp(2,:) = add_scalar_to_matrix(c_in(2,:),y_shift);
    
% Ausgabe
    c_out       = c_temp;
end


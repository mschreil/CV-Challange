function [koords] = grid2lin(x_grid,y_grid)
%GRID2LIN berechnet die einzelnen Koordinaten eines Meshgrids
% [koords] = grid2lin(x_grid,y_grid)
% x_grid:           erste Ausgabe aus meshgrid    (m x n)
% y_grid:           zweite Ausgabe aus meshgrid   (m x n)
% koords:           alle Koordinaten              (2 x m*n)

% Ausgabe 
    koords      = [x_grid(:) y_grid(:)]';
end


function [m_ir1,m_ir2,c_change] = match_rect_sizes(irect1,irect2)
%MATCH_RECT_SIZES Nullzeilen - und Spalten so anf�gen, dass die beiden
%rektivizierten Bilder gleichgro� sind. Das bedeutet die Nullzeilen und
%Spalten werden an das kleinere Bild angef�gt
% [m_ir1,m_ir2,c_change] = match_rect_sizes(irect1,irect2)
% m_ir1             Ausgabe matched-size-image 1
% m_ir2             Ausgabe matches-size-image 2
% irect1            Rektiviziertes Bild links (Ausgabe aus rect.m)
% irect2            Rektiviziertes Bild rechts (Ausgabe aus rect.m)
% c_change          Koordinatenver�nderung (ben�tigt falls Koordinaten
%                   im neuen Bild, das nurch die nan  in der Dimension 
%                   ver�ndert wurde, berechnet werden sollen) 
%                   [yl+ xl+;
%                    yr+ xr+] (Zahlen die auf alte Koordinaten addiert
%                   werden m�ssen) (yl: Zahl die auf linkes Bild addiert
%                   muss usw.)

% Initialisierung
    c_change    = zeros(2,2);

%% Dimensionen ausgleichen durch anf�gen in y-Richtung
    diff_y           = size(irect1,1) - size(irect2,1);
    if diff_y < 0
        % Bild 2 ist h�her als Bild 1
        im1addu8y    = [nan(floor(-diff_y/2),size(irect1,2),size(irect1,3));...
                                    irect1;...
                        nan(ceil(-diff_y/2),size(irect1,2),size(irect2,3))];
        im2addu8y    = irect2;
        c_change(1,1)= floor(-diff_y/2); 
    else
        % Bild 1 ist h�her als Bild 2
        im1addu8y    = irect1;
        im2addu8y    = [nan(floor(diff_y/2),size(irect2,2),size(irect2,3));...
                                    irect2;...
                        nan(ceil(diff_y/2),size(irect2,2),size(irect2,3))];
        c_change(2,1)= floor(diff_y/2);
    end
    
    % Anzeigen
    visualize = 0;
    if visualize
        figure;
        hold on
        imshow([im1addu8y im2addu8y])
        hold off
    end
   
%% Dimensionen ausgleichen durch anf�gen in x-Richtung
    
    % Dimensionen Bild links mit y - Ausgleich
    [ydim1,xdim1,zdim1]     = size(im1addu8y);
    [ydim2,xdim2,zdim2]     = size(im2addu8y);
    
    % Ausgleichen der X - Dimension
    diff_x                  = xdim1 - xdim2;
    if diff_x < 0
        % Bild 2 ist breiter als Bild 1
        im1addu8x           = [nan(ydim1,floor(-diff_x/2),zdim1),...
                                            im1addu8y,...
                               nan(ydim1,ceil(-diff_x/2),zdim1)];
        % Bild 2 bleibt gleich
        im2addu8x           = im2addu8y;
        c_change(1,2)       = floor(-diff_x/2);
    else
        % Bild 1 ist breiter als Bild 2
        im1addu8x           = im1addu8y;
        im2addu8x           = [nan(ydim2,floor(diff_x/2),zdim2),...
                                            im2addu8y,...
                               nan(ydim2,ceil(diff_x/2),zdim2)];
        c_change(2,2)       = floor(diff_x/2);
    end
    
    % Anzeigen
    visualize = 0;
    if visualize
        figure;
        hold on
        imshow([im1addu8x im2addu8x])
        hold off
    end
        
m_ir1 = im1addu8x;
m_ir2 = im2addu8x;
end


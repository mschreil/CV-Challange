function [x_normed] = normalize(x)
%NORMALIZE Normiert dritte Zeile auf f ( = normalerweise 1) 
% h�ngt von globaler Variable f ab!
%   [x_normed] = normalize(x)
% x:        Vektor bzw. Matrix der dritte Zeile auf 1 normiert werden soll 
%           (3 x n)
% x_normed: Normierte Matrix von x (3 x n)

f = 1;

if eq(3,size(x,1))
    norm_fak = f * ones(1,size(x,2)) ./ x(3,:);
    x_normed = norm_fak .* x;
else
    error('x hat nicht Dimension (3 x n)');
end

end


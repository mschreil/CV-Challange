function [cp_r] = calc_rect_corners(size_img,H,g1x,g1y)
%CALC_RECT_CORNERS berechnet die Eckkoordinaten eines rectifizierten 
%Bildes
% [cp_r] = calc_rect_corners(size_img,H,g1x,g1y)
% size_img:             Vektor mit Gr��e des Bildes ([ydim,xdim]), 
%                       entspricht size(img), (2 x 1)
% H:                    Transformationsmatrix mit der das Bild 
%                       rektiviziert wurde
% cp_r:                 Berechnete Eckpunkte (Start links oben, gegen
%                       den Uhrzeigersinn angeordnet), erste Zeile 
%                       X - Koordinaten und zweite Zeile 
%                       Y - koordinaten, (2 x 4)
% g1x,g1y:              Meshgrid f�r X und Y 
%                       (erzeugt durch get_new_grid), beide (n x m) 

% Eckpunkte im nicht rektivizierten Bild
    % Dimensionen
    ydim        = size_img(1);
    xdim        = size_img(2);
    % Eckpunkte
    cp          = [[1;1],[1;ydim],[xdim;ydim],[xdim;1]];
    
% Eckpunkte im rektivizierten Bild
    cp_r        = calc_rect_coords(g1x,g1y,cp,H);
    
end


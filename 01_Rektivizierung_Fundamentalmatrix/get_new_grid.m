function [g_x,g_y] = get_new_grid(size_img,H)
%GET_NEW_GRID berechnet das Grid eines transformierten Bildes (das Grid,das
%zur L�sung des Optimierungsproblem verwendet wird und NICHT das Grid mit
%dem durch Interpolieren die Intensit�tswerte berechnet werden)
% [g_x,g_y] = get_new_grid(size_img,H)
% size_img:     Gr��e des Bild, das transformiert werden soll 
% H:            Transformationsmatrix mit der img gewarpt werden soll
% g_x           Meshgrid f�r X des neuen Bildes
% g_y           Meshgrid f�r Y des neuen Bildes

% Dimensionen des Bildes img
    x_dim       = size_img(2);
    y_dim       = size_img(1);

% Berechne Eckpunkte des gewarpten Bild in Bildkoordinaten (BP)
    % Links oben (l.o.)
    lo          = normalize(H * [1,1,1]');         % [1,1,1]' ist l.o. in BP
    % Rechts oben
    ro          = normalize(H * [x_dim,1,1]');
    % Links unten
    lu          = normalize(H * [1,y_dim,1]');
    % Recht unten 
    ru          = normalize(H * [x_dim,y_dim,1]');
    % Corner points array
    corners     = [lo,ro,lu,ru];
    
% Berechnen kleines m�gliches, rechteckiges Fenster, das das gewarpte Bild
% enthalten kann
    x_min       = min(corners(1,:));
    y_min       = min(corners(2,:));
    x_max       = max(corners(1,:));
    y_max       = max(corners(2,:));
    
% Berechne Grid des verherigen Fensters
    [g_x,g_y]   = meshgrid(x_min:x_max,y_min:y_max);
end


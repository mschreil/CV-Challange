function [coord,dummy] = calc_rect_coords(grid_x,grid_y,coords_in,H)
%CALC_RECT_COORDS Berechnunge der Koordinaten (Indexe) im Grid des rectifizierten 
%Bildes die am n�hesten sind bzw. am besten passen. Verwendung      
%ausschlie�lich in VISUALIZE_MATCHES_RECT.
% [coord,dummy] = calc_rect_coords(grid_x,grid_y,coords_in,H)
% grid_x:       x - Grid des rektivizierten Bildes (aber new Grid -> 
%               also das Grid, das durch get_new_grid ensteht!)
% grid_y:       y - Grid des rektivizierten Bildes (aber new Grid -> 
%               also das Grid, das durch get_new_grid ensteht!)
% coords_in:    Koordinate im normal, nicht rektivizierten Bild (2 x n)
% coord:     Koordinate (Indici) im rektivizierten Bild (2 x n)
% size_img:     Gr��e des Bildes aus dem die Koordinaten coords_in sind
%               (entspricht size(img))
% H:            Transformationsmatrix H

% F�r mehrere Koordinaten
        P           = [coords_in;ones(1,size(coords_in,2))];

% Transformieren
    P_t         = normalize(H * P);

% Berechnung
    % Initialisierung der Ausgangsvariablen wg. for - Schleife
    coord       = zeros(size(coords_in));
for loop = 1:size(coords_in,2)
    % Finde Punkt im neuen Grid
        % Abstand zu jedem Punkt im Grid
        grid_lin    = double(grid2lin(grid_x,grid_y));
        dist        = grid_lin - P_t(1:2,loop) * ones(1,size(grid_lin,2));
        norm_dist   = vecnorm(abs(dist),2,1);

        % Minimierung des Abstandes
        [~,i]       = min(norm_dist);
        [j,k]       = ind2sub(size(grid_x),i);

    % Koordinate im Bild/R�ckgabe
        coord(:,loop)= [k;j];
end
% dummy damit alte Skripte noch funktionieren
    dummy       = NaN;
end


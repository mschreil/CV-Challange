function merkmale = harris_detektor(input_image, varargin)
    % In dieser Funktion soll der Harris-Detektor implementiert werden, der
    % Merkmalspunkte aus dem Bild extrahiert
        %% Input parser
    default_segment_length=15;
    default_k=0.05;
    default_tau=1e6;
    default_do_plot=false;
    default_min_dist=20;
    default_tile_size=[200,200];
    default_N=5;
    
    valid_length= @(x) isnumeric(x) && (x > 1) && (mod(x,2)~=0);
    valid_k= @(x) isnumeric(x) && (x >= 0) && (x <= 1);
    valid_tau = @(x) isnumeric(x) && (x > 0);
    valid_do_plot = @(x) islogical(x);
    valid_min_dist = @(x) isnumeric(x) && (x >= 1);
    valid_tile_size = @(x) isnumeric(x);
    valid_N= @(x) isnumeric(x) && (x >= 1);
      
    p = inputParser;
    
    addRequired(p,'input_image');
    addOptional(p,'segment_length',default_segment_length,valid_length);
    addOptional(p,'k',default_k,valid_k);
    addOptional(p,'tau',default_tau,valid_tau);
    addOptional(p,'do_plot',default_do_plot,valid_do_plot);
    addOptional(p,'min_dist',default_min_dist,valid_min_dist);
    addOptional(p,'tile_size',default_tile_size,valid_tile_size);
    addOptional(p,'N',default_N,valid_N);
    
    parse(p,input_image,varargin{:});
    tile_size=p.Results.tile_size;
    if(numel(p.Results.tile_size)==1)
        tile_size=[tile_size,tile_size];
    end
    
    segment_length=p.Results.segment_length;
    k=p.Results.k;
    tau= p.Results.tau;
    do_plot=p.Results.do_plot;
    min_dist=p.Results.min_dist;
    tile_size=p.Results.tile_size;
    N=p.Results.N;
    
    %% Vorbereitung zur Feature Detektion aus Aufgabe 1.4
        dim=ndims(input_image);    %Anzahl der Dimensionen des Eingabebildes bestimmen
    
    if (dim~=2)
        error('Image format has to be NxMx1');
    else
        input_image=double(input_image(1:end,1:end,1)); 
    % Approximation des Bildgradienten
       x=[1 0 -1;2 0 -2;1 0 -1];  %Faltungsmatrix in X-Richtung
       y=[1 2 1; 0 0 0;-1 -2 -1];  %Faltungsmatrix in Y-Richtung
       Fx=conv2(input_image,x,'same'); %Bildgradient in X-Richtung mittels Faltung mit Faltungsmatrix mit gleicher Größe bestimmen
       Fy=conv2(input_image,y,'same'); %Bildgradient in Y-Richtung mittels Faltung mit Faltungsmatrix mit gleicher Größe bestimmen
    % Gewichtung
        w   = fspecial('gaussian',[segment_length,1],segment_length/3);
    % Harris Matrix G
    G11  = double(conv2(w,w,Fx.^2, 'same'));
    G22  = double(conv2(w,w,Fy.^2, 'same'));
    G12  = double(conv2(w,w,Fx.*Fy, 'same'));
    
    
    %% Merkmalsextraktion ueber die Harrismessung aus Aufgabe 1.5
     H   = ((G11.*G22 - G12.^2) - k*(G11 + G22).^2);
    %% Merkmalsextraktion ueber die Harrismessung
    m=zeros(size(H));
    m((ceil(segment_length/2)+1):(size(H,1)-ceil(segment_length/2)),(ceil(segment_length/2)+1):(size(H,2)-ceil(segment_length/2)))=1;
    corners = H.*m;
    %Schwellwertbildung der Merkmale
    corners(corners<=tau)=0;
    [row,col]=find(corners);
    merkmale_xy=[col.';row.'];
    
    %% Merkmalsvorbereitung aus Aufgabe 1.9
    m=zeros(size(corners,1)+2*min_dist,size(corners,2)+2*min_dist);
    m((min_dist+1):(size(m,1)-min_dist),(min_dist+1):(size(m,2)-min_dist))=corners;
    corners=m;
    
    % Merkmale absteigend sotieren
    [B,I] = sort(corners(:),'descend');

    % Merkmale mit Eintrag 0 werden eleminiert
    I(B==0)=[];
    sorted_index=I;
    
    %% Akkumulatorfeld aus Aufgabe 1.10
    anz_merkmale = numel(sorted_index);
    size_corners = size(corners);

    % Erstellen des Akkumulator-Arrays mit Nullen befüllt
    AKKA = zeros(ceil(size(input_image,1)/tile_size(1)),ceil(size(input_image,2)/tile_size(2)));

    % Matrix zum speichern der Merkmale
    merkmale=zeros(2,min(numel(AKKA)*N,anz_merkmale));
    
    %% Merkmalsbestimmung mit Mindestabstand und Maximalzahl pro Kachel
    % Anzahl an Merkmalen ungleich null und Größe des Suchfeldes
    anz_merkmale = numel(sorted_index);
    size_corners = size(corners);
    
    % Variable zum zählen der Merkmale
    count_merkmale=1;
    
    %Kuchenmatrix
    [x,y]=meshgrid(-min_dist:min_dist,[-min_dist:-1,0:min_dist]);
    cake_matrix=sqrt(x.^2+y.^2)>min_dist;

    for  i = 1:anz_merkmale
        % Nehme nächstes Element aus der sortierten Liste
        index = sorted_index(i);
        % Überprüfen ob dieses Element null ist   
        if(corners(index)==0)
                continue;
        else
               % Reihen- und Spaltenindex berechnen
               col = floor(index/size_corners(1));
               row = index - col*size_corners(1);
               col = col + 1;
        end
        
        %Indizies den Kachelnkoordinaten zuweisen
        Ex = floor((row-min_dist-1)/(tile_size(1)))+1;
        Ey = floor((col-min_dist-1)/(tile_size(2)))+1;
        
        % Inkrementieren des Akkumulatorarrays
        AKKA(Ex,Ey)=AKKA(Ex,Ey)+1;
        
        % Merkmalpunkt elementweise mit Kuchen-Matrix multiplizieren
        corners(row-min_dist:row+min_dist,col-min_dist:col+min_dist)=corners(row-min_dist:row+min_dist,col-min_dist:col+min_dist).*cake_matrix;
        
        %Überprüfen ob Akkumulatorarray schon N Elemente beinhaltet
        if AKKA(Ex,Ey)==N
            %Falls ja alle anderen Merkmale zu 0 setzen
            corners((((Ex-1)*tile_size(1))+1+min_dist):min(size(corners,1),Ex*tile_size(1)+min_dist),(((Ey-1)*tile_size(2))+1+min_dist):min(size(corners,2),Ey*tile_size(2)+min_dist))=0;   
        end
        
        %Koordinaten des Merkmals in Array merkmale speichern (Ohne Nullrand)
        Merkmale(:,count_merkmale)=[col-min_dist;row-min_dist];
        % Merkmalszähler inkrementieren
        count_merkmale = count_merkmale+1;
    end
    
    % Plot Routine
%     figure  
%         colormap('gray')
%         imagesc(input_image)
%         hold on;
%         plot(merkmale(1,:), merkmale(2,:), 'gs');
%         plot(merkmale(1,:), merkmale(2,:), 'g.');
%         axis('off');
    
    merkmale=Merkmale;
end


function [F,mP1_inliers,mP2_inliers] = get_F(img1,img2,visualize)
%GET_F Berechnung der Fundamentalmatrix mit Matlab Funktionen
% [F,mP1,mP2] = get_F(img1,img2,visualize)
%   Sp�ter Ersetzen durch eigene Funktionen
% F:            R�ckgabe der berechneten Fundamentalmatrix
% img1:         Graustufenbild links
% img2:         Graustufenbild rechts
% visualize:    Zeige Epipolarlinien
% mP1_inliers:  Unkalibrierte Koordinaten der Matched Points von Bild 1
%               (im Consensius set enthalten, inliers -> robust) (2 x n)
% mP2_inliers:  Unkalibrierte Koordinaten der Mateched Points von Bild 2
%               (im Consensius set enthalten, inliers -> robust) (2 x n)

img1=rgb_to_gray(img1);
img2=rgb_to_gray(img2);

Merkmale1 = harris_detektor(img1,'segment_length',5,'k',0.05,'tau',1e6,'min_dist',15,'N',500,'do_plot',false);
Merkmale2 = harris_detektor(img2,'segment_length',5,'k',0.05,'tau',1e6,'min_dist',15,'N',500,'do_plot',false);

Korrespondenzen = punkt_korrespondenzen(img1,img2,Merkmale1,Merkmale2,'window_length',21,'min_corr',0.98,'do_plot',false);

Korrespondenzen_robust = F_ransac(Korrespondenzen,'epsilon',0.75,'p',0.75 ,'tolerance', 0.1);

mP1_inliers = Korrespondenzen_robust(1:2,1:end);
mP2_inliers = Korrespondenzen_robust(3:4,1:end);

%% Zeige die robusten Korrespondenzpunktpaare
%     Korrespondenzen_plot=sortrows(Korrespondenzen_robust',[1,2])';
%     colors=char('blue','red','green','magenta','yellow','cyan');
%     markers=char('o','s','^','v','>','<','p','h');
%     marker_size=7;
%     font_size=8;
%     offset=2*font_size+marker_size+1;
%     figure;
%     imshow([img1,img2]);
%     hold on;
%     for i=1:size(Korrespondenzen_plot,2)
%         plot(Korrespondenzen_plot(1,i),Korrespondenzen_plot(2,i),strcat(colors(mod(i,size(colors,1))+1,:),markers(mod(i,size(markers,1))+1,:)),'MarkerSize',marker_size)
%         text(Korrespondenzen_plot(1,i),Korrespondenzen_plot(2,i)+offset,num2str(i),'Color',colors(mod(i,size(colors,1))+1,:),'FontSize',font_size)
%         plot(Korrespondenzen_plot(3,i)+size(img1,2),Korrespondenzen_plot(4,i),strcat(colors(mod(i,size(colors,1))+1,:),markers(mod(i,size(markers,1))+1,:)),'MarkerSize',marker_size)
%         text(Korrespondenzen_plot(3,i)+size(img1,2),Korrespondenzen_plot(4,i)+offset,num2str(i),'Color',colors(mod(i,size(colors,1))+1,:),'FontSize',font_size)
%         line([Korrespondenzen_plot(1,i),Korrespondenzen_plot(3,i)+size(img1,2)],Korrespondenzen_plot([2,4],i),'Color',colors(mod(i,size(colors,1))+1,:));
%     end

%% Berechne die Fundamental-Matrix
F = achtpunktalgorithmus(Korrespondenzen_robust);
%disp(F);

end


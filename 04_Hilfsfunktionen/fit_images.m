function [Iout1,Iout2] = fit_images(Iin1, Iin2, edges1, edges2)
%Verschieben und zuschneiden des Rectifizierten Bildes
%f�r edges1 und edges2 Ecken m�ssen in edges gegen den Uhrzeigersinnn abgespeichert sein

% Iin1    = Rectifiziertes Eingangsbild1 mit NaN Rand
% Iin2    = Rectifiziertes Eingangsbild2 mit NaN Rand 
% edges 1 = Eckpunkte des Bildes 1
% edges 2 = Eckpunkte des Bildes 2 

%Datenformat edges1 und eges 2 
%1row=xkoordianten 
%2row=ykoordinaten 

upperPoints1 = edges1(:,1:3:4);
lowerPoints1 = edges1(:,2:3);
upperPoints2 = edges2(:,1:3:4);
lowerPoints2 = edges2(:,2:3);

upperCut1 = max(upperPoints1(2,:));
lowerCut1 = min(lowerPoints1(2,:));
upperCut2 = max(upperPoints2(2,:));
lowerCut2 = min(lowerPoints2(2,:));

upperMax = max(upperCut1, upperCut2);
lowerMin = min(lowerCut1, lowerCut2);

Iout1 = Iin1(upperMax:lowerMin,:,:);
Iout2 = Iin2(upperMax:lowerMin,:,:);

%Iout1 = Iin1(upperCut1:lowerCut1,:,:);
%Iout2 = Iin2(upperCut2:lowerCut2,:,:);

[iy1,ix1,iz1] = size(Iout1);
[iy2,ix2,iz2] = size(Iout2);


%% fit image width to the smalest possible value for each image

% [left1, right1] = calc_border(Iout1);
% [left2, right2] = calc_border(Iout2);
% 
% leftMin1 = min(left1)+1;                %+1 um von anz auf index zu konvertieren (start bei 0, start bei 1)
% rightMin1 = min(right1)+1;
% 
% leftMin2= min(left2)+1;
% rightMin2 = min(right2)+1;
% 
% Iout1 = Iout1(:,leftMin1:ix1-(rightMin1-1),:);
% Iout2 = Iout2(:,leftMin2:ix2-(rightMin2-1),:);


%% fit Image to same width
% (wird von rect-Funktion �bernommen)
%
% maxX = max(ix1,ix2);
% 
% temp1 = NaN(iy1,maxX,iz1);
% temp1(:,1:ix1,:) = Iout1;
% Iout1 = temp1;
% 
% temp2 = NaN(iy2,maxX,iz2);
% temp2(:,1:ix2,:) = Iout2;
% Iout2 = temp2;

%% fit Image to same hight 

yDiff_h = abs(iy1-iy2)/2; 

[iy1,ix1,iz1] = size(Iout1);
[iy2,ix2,iz2] = size(Iout2);

%--------------------------------------------------------------------------
% erweitere zu kleines Bild mit NaN Werten
%--------------------------------------------------------------------------

% if iy1 > iy2
%     
%     temp = NaN(iy1,ix2,iz2);
%     temp(ceil(yDiff_h):iy2+floor(yDiff_h)-1,:,:) = Iout2;
%     Iout2 = temp;
% end
% 
% 
% if iy1 < iy2
%     temp = NaN(iy2,ix1,iz1);
%     temp(ceil(yDiff_h):iy1+floor(yDiff_h)-1,:,:) = Iout1;
%     Iout1 = temp;
% end 

%--------------------------------------------------------------------------
% beschneide zu gro�es Bild 
%--------------------------------------------------------------------------

if iy1 > iy2
    Iout1= Iout1(ceil(yDiff_h):iy1-floor(yDiff_h)-1,:,:);
end

if iy1 < iy2
    Iout2= Iout2(ceil(yDiff_h):iy2-floor(yDiff_h)-1,:,:);
end
 
 
%% schiebe Bilder nach links und beschneide auf gleiche Gr��e ohne Rand 

[left1, ~] = calc_border(Iout1);
[left2, ~] = calc_border(Iout2);
Iout1 = shift_rows(Iout1,left1, false);
Iout2 = shift_rows(Iout2,left2, false);

[~, right1] = calc_border(Iout1);
[~, right2] = calc_border(Iout2);

maxWidth1 = ix1 - min(right1);
maxWidth2 = ix2 - min(right2);

diffWidth = maxWidth1 - maxWidth2;

if diffWidth > 0 
    
    factor = (maxWidth2 + diffWidth)/maxWidth2;
    Iout2 = distort_in_x(Iout2,factor);

else

    factor = (maxWidth1 + abs(diffWidth))/maxWidth1;
    Iout1 = distort_in_x(Iout1,factor);    
    
end

[~, right1] = calc_border(Iout1);
[~, right2] = calc_border(Iout2);
Iout1 = Iout1(:,1:size(Iout1,2)-max(right1),:);
Iout2 = Iout2(:,1:size(Iout2,2)-max(right2),:);

[iy1,ix1,iz1] = size(Iout1);
[iy2,ix2,iz2] = size(Iout2);

minX = min(ix1,ix2);
Iout1 = Iout1(:,1:minX,:);
Iout2 = Iout2(:,1:minX,:);
end
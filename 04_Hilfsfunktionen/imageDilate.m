function out = imageDilate(bin_img,s)
% Dilation des Eingangsbilde mit dem Filterkern S 

%bin_img = Eingangsibld -> muss ein Bin�res Bild sein 
%s = filterkern

%out = gefilters Bild


% -----------for Testing--------------
%bin_img = [0 0 1 0; 
%           1 0 1 0; 
%           1 1 1 0; 
%           0 0 1 1;]

%s = [0 1 0; 
%     1 1 1;
%     0 1 0]
%
%-------------------------------------


[r n]=size(bin_img);
[p q]=size(s);
a = floor(p/2);
b = floor(q/2);

out = zeros(r+2*a,n+2*b);

for i=1:r
   for j=1:n
       if (bin_img(i,j) == 1)
           
           for k=1:p
                for l=1:q
                    if(s(k,l)==1)
                        c = (i + k) - a;
                        d = (j + l) - b;
                        out(c,d) = 1;
                    end
                end
           end
           
       end
    end
end
out = out(a+1:r+1,b+1:n+1);



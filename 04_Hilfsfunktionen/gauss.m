function OutputImage = gauss(ImageData,WindowSize,SigmaValue)

% Initialisize the grid
[X,Y]=meshgrid(-WindowSize:WindowSize,-WindowSize:WindowSize);

%Gauss-Filter
Temp = -(X.^2+Y.^2)/(2*SigmaValue*SigmaValue);
FinalCalculation= exp(Temp)/(2*pi*SigmaValue*SigmaValue);

% Perform convolution
OutputImage = conv2(ImageData, FinalCalculation, 'same');

end


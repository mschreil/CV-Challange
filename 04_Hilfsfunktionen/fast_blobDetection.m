function [labels_new, anzLabels, PixelIdxList] = fast_blobDetection(bin_img)

    labels = sp_fast_connected_relabel(double(bin_img));

    [N,edges] = histcounts(labels,max(max(labels)));
    index = find(N==max(N));
    temp = labels+1;
    temp(temp == index) = 0;
    labels_new = reshape(temp,size(labels));
    sub_mat = labels_new > index;
    labels_new = labels_new - sub_mat;

    temp = labels_new;
    temp(temp == 0) = [];
    temp = sort(temp);
    anzLabels = max(temp);

    PixelIdxList = cell(1,anzLabels); 
    
    for j = 1:anzLabels
        temp = find(labels_new==j);
        PixelIdxList{j} = temp;
    end

end 


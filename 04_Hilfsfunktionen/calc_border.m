function [left, right] = calc_border(img)
% Berechnen der Anzahl von NaN werten auf der rechten und linken Seite des
% Bildes f�r jede Reihe des Bildes 


% ----- for testing -----
% img = NaN(20);
% A = randi(10,18,12);
% img(2:19,7:18) = A;
% -----------------------

[iy,ix,iz] = size(img);

if iz > 1 
    img=img(:,:,1);
end 

% img_left = img(:,1:round(ix/2));
% img_right = img(:,round(ix/2)+1:ix);
% 
% nan_left = isnan(img_left);
% nan_right = isnan(img_right);
% 
% left = sum(nan_left,2);
% right = sum(nan_right,2);
% 
% %schlie�e zeilen ohne Valid-Pixel beim shift aus 
%  left(left == round(ix/2)) = 0;
%  right(right == round(ix/2)) = 0;

nans = isnan(img);
diffs = diff(nans,1,2);
diffs = (diffs ~= 0);

right = zeros(iy,1);
left  = zeros(iy,1);

for jj = 1:iy
    
   temp = find(diffs(jj,:));
   
   if(isempty(temp))
       continue;
       % 1�0 empty double row vector Problem
   else
    right(jj) = ix - max(temp);
    left(jj) = min(temp);
   end 
end 


% index = find(diffs);
% [r,c] = ind2sub(size(diffs), index);
% 
% right = zeros(iy,1);
% left  = zeros(iy,1);
% 
% left(r(1:ceil(length(r)/2))) = c(1:ceil(length(c)/2));
% right(r(floor(length(r)/2):end)) = c(floor(length(c)/2):end);
% 
% right = ix - right; 

 

end 



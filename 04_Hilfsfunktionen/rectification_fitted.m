function [i1, i2, stereoImage] = rectification_fitted(image1, image2, offset_on)

    disp('Rektifizieren der Bilder')
    
    %try
    %for i = 1:5
        [ir1,ir2,grids,corners] = rect(image1,image2,offset_on);
        ir1 = ir1/255;
        ir2 = ir2/255;
        
        %visualize_matches_one_img(corners.left,ir1);
        %visualize_matches_one_img(corners.right,ir2);
        
%          stereoImage1 = ir1;
%          stereoImage2 = ir2; 
%          stereoImage1(:,:,1) = 0;
%          stereoImage2(:,:,2:3) = 0;
%          stereoImage = stereoImage1 + stereoImage2;
%          figure; imshow(stereoImage);
%          hold on;
%          for ii = 1:50:size(ir1,1)
%          line([1 size(ir1,2)],[ii ii],'Color','red'); 
%          end 
%          hold off;
     
     
         [i1,i2] = fit_images(ir1, ir2, corners.left, corners.right);
         
%           stereoImage1 = i1;
%           stereoImage2 = i2; 
%           stereoImage1(:,:,1) = 0;
%           stereoImage2(:,:,2:3) = 0;
%           stereoImage = stereoImage1 + stereoImage2;
%           figure; imshow(stereoImage);
%           hold on;
%           for ii = 1:50:size(i1,1)
%           line([1 size(i1,2)],[ii ii],'Color','red'); 
%           end 
%           hold off;

%    end 
    
%        [y1,x1,z1] = size(i1);
%        [y2,x2,z2] = size(i2);
  
   
    %catch 
    %    y1 = 1;
    %    x1 = 1;
    %    y2 = 1;
    %    x2 = 1;
    %end 
    
%    a = 2;
    
%     if((y1 < 2000/a || x1 < 3000/a) || ( y2 < 2000/a ||  x2 < 3000/a ))
%     
%        while true
%            
%             disp('Rektifizieren der Bilder gescheitert, erneutes Rektifizieren')
%             try 
%                 [ir1,ir2,grids,corners] = rect(image1,image2,offset_on);
%                 ir1 = ir1/255;
%                 ir2 = ir2/255;
%                 [i1,i2] = fit_images(ir1, ir2, corners.left, corners.right);
% 
%                 [y1,x1,z1] = size(i1);
%                 [y2,x2,z2] = size(i2);
% 
%                 if((y1 > 2000/a && x1 > 3000/a) && ( y2 > 2000/a &&  x2 > 3000/a ))
%                     break;
%                 end 
%             catch 
%                 continue
%             end 
%        end 
%        
%     end 
    
    
    stereoImage1 = i1;
    stereoImage2 = i2; 
    
    stereoImage1(:,:,1) = 0;
    stereoImage2(:,:,2:3) = 0;
    stereoImage = stereoImage1 + stereoImage2;

end 
function output = medianfilter(InputImage)
%MEDIANFILTER Summary of this function goes here
%   Detailed explanation goes here
    a = double(InputImage);
    b = a;
    
    [row, colm] = size(InputImage);
    for i = 2:1:row-1
        for j = 2:1:colm-1
            %f=a(i,j)+((a(i-1,j)+a(i+1,j)-2.0*a(i,j))/(2.0*(1+2.303^(-10000.0*(a(i,j)-a(i-1,j))*(a(i,j)-a(i+1,j)))))+(a(i,j-1)+a(i,j+1)-2.0*a(i,j))/(2.0*(1+2.303^(-10000.0*(a(i,j)-a(i,j-1))*(a(i,j)-a(i,j+1))))))/2.0;
            a1 = [a(i-1,j-1) a(i-1,j) a(i-1,j+1) a(i,j-1) a(i,j) a(i,j+1) a(i+1,j-1) a(i+1,j) a(i+1,j+1)];
            a2 = sort(a1);
            med = a2(5); 
            %b(i,j) = (0.9*med+0.1*f);
            b(i,j) = med;
        end
    end
    output=b;
end


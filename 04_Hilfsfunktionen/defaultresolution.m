function Output = defaultresolution(img,colmsoll,rowsoll)
%DEFAULT Summary of this function goes here
%   Detailed explanation goes here
disp('Anpassung der Aufl�sung');

%Dimensionen des Input-Bildes bestimmen
[rowinput,colminput,diminput] = size(img);

%Verhältnis von gewünschen Zeilenanzahl zur tatsächlichen Zeilenanzahl
faktor = rowsoll/rowinput;

%Bild hochskalieren damit gewünschte Zeilenanzahl erreicht wird
outtemp=img_resize(img,faktor);
[rowout, colmout,dimout] = size(outtemp);

%Wegschneiden der überflüssigen Spalten um gewünschte Spaltenanzahl zur
%erreichen
diffcol=colmout-colmsoll;
halbdiff=diffcol/2;
if ((mod(diffcol,2))==0)
    Output=outtemp(:,halbdiff+1:colmout-halbdiff,:);
else
    halbdiff=uint8(halbdiff);
    halbdiff=double(halbdiff);
    Output=outtemp(:,halbdiff:colmout-halbdiff,:);
end
end


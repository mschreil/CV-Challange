function [img_out,range_x] = distort_in_x(img,factor)
%DISTORT_IN_X Verzerren so, dass Bilder gleichbreit
%   Detailed explanation goes here

% Dimensionen
[ydim,xdim,zdim]        = size(img);

% Neues Grid
range_x                 = linspace(1,xdim,round(factor * xdim));
range_y                 = 1:ydim;
[gx,gy]                 = meshgrid(range_x,range_y);

% für Farbild
gx_rgb                  = repmat(gx,1,1,zdim);
gy_rgb                  = repmat(gy,1,1,zdim);

% Neue Intensitätswerte durch Interpolation
img_out                 = zeros(size(gx_rgb));
for i = 1:zdim
    img_out(:,:,i)      = interp2(double(img(:,:,i)),gx_rgb(:,:,i),...
                                                     gy_rgb(:,:,i));
end

img_out                 = img_out;
% img_out                 = (img_out);
end


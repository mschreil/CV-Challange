function [B] = faltungs_filter(I,h)
% I ... 2D image
% h ... 2D filter
% B ... returns filtered 2D image
%filter ... multiplicates every element of I with h and sums output of
%every element

% do the two-dimensional convolution
B = conv2(I, h, 'same');

end
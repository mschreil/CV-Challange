function I = shift_rows(I,shift, direction)
% schiebt jede Reihe des Bildes um den im Shiftvektor angeaben Wert in die
% angegebene Richtung; 

% shift     =  shift Vektor = Enth�tl verschiebungswerte, muss die gleiche l�nge
%              besitzen, wie das bild Reihen enth�lt 

% direction = richtung in die geschiftet wird, wenn TRUE dann shift nach
%             rechts, wenn FALSE dann shift nach links 


[i1,i2,~] = size(I);
[s1, ~] = size(shift); 

if s1 ~= i1 
    error("length of shift Vektor doesn't fit the length of Image columns")
end 

if direction == false
    %disp('shift to left border')
    for ii = 1:i1
        I(ii, 1:i2-shift(ii),:) =  I(ii, shift(ii)+1:i2, :);
        I(ii,i2-(shift(ii)-1):i2, :) = NaN;
        %I(ii,i2-(shift(ii)-1):i2, :) = 0;
    end
end 


if direction == true
    %disp('shift to right border')
    for ii = 1:i1
        I(ii, shift(ii)+1:i2, :) = I(ii, 1:i2-shift(ii), :);   
        I(ii,1:shift(ii), :) = NaN; 
        %I(ii,1:shift(ii), :) = 0; 
        
    end

end 
end 
    
    
    
    
  
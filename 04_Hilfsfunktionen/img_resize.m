function [img_out] = img_resize(img,factor)
%IM_RESIZE Ersatz der imresize Matlab - Image Processing Toolbox
% factor

% Dimensionen
[ydim,xdim,zdim]        = size(img);

% Neues Grid
x_range                 = linspace(1,xdim,round(factor * xdim));
y_range                 = linspace(1,ydim,round(factor * ydim));
[gx,gy]                 = meshgrid(x_range,y_range);

% für Farbbild
gx_rgb                  = repmat(gx,1,1,zdim);
gy_rgb                  = repmat(gy,1,1,zdim);

% Neue Intensitätswerte durch Interpolation
img_out                 = zeros(size(gx_rgb));
for i = 1:zdim
    img_out(:,:,i)      = interp2(double(img(:,:,i)),gx_rgb(:,:,i),...
                                                     gy_rgb(:,:,i));
end

% img_out                 = img_out;
end


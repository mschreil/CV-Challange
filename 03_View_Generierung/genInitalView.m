function [imgOut, refEdges]  = genInitalView(p, img1, img2, d1, d2)
% Erzeugt einen Viritul View zwischen zwei rectifizierten Bildern und ein bin�res  
% Bild, welches eine 1 an allen stellen bestitz wo Pixel pixel aus den biden bildern zusammengef�hrt wurden,
% dieses bin�re Bild wird im n�chsetn schritt verwendten
% um fehlerhafte Pixel zu beseitigen

% p    = Werte zwischen 0 und 1 der die Postionen des generiereten Bildes zwischen den beiden vorgegebenen Bilder angibt 
% img1 = linkes Bild 
% img2 = rechts Bild 
% d1   =  Disparity bezogen auf das linke Bild 
% d2   =  Dispartiy bezogen auf das rechte Bild 

% imgOut = berechnets intermediate Bild
% refEdges = zeigt postione fehlerhafter Pixel (wird ben�tig f�r Refinment)

%% Terminalausgabe
disp('Berechnung der Virtuellen Ansicht')

%% init Varibales
[sy, sx, sz] = size(img1);
disp1 = double(d1);
disp2 = double(d2);
disp1(disp1 == 0) = nan;
disp2(disp2 == 0) = nan;

imgInt1Relevant = nan(sy,sx,sz);
imgInt2Relevant = nan(sy,sx,sz);
dispInt1 = nan(sy,sx);
dispInt2 = nan(sy,sx);

isDisp1 = ~isnan(disp1);
isDisp2 = ~isnan(disp2(:,end:-1:1));

%% generiert Intermediate Left and Right Image and Disparity

%berechnen zweier matirzen die die verschiebungswerte der einzelenen
%Bildpixel in x Richtung f�r imgInt1 und imgInt2 enthalten, min wert = 1 und maxwert = sx
%Formeln siehe paper "3.1 Generating the initial View"
x1p = min(sx, max(1,repmat((1:sx),[sy 1]) - round(disp1.*p)));
x2p = min(sx, max(1,repmat(sx:-1:1,[sy 1]) + round(disp2(:,sx:-1:1)*(1-p))));

for y = 1:sy
    %nur Werte f�r die auch ein Disparity Wert existiert
    ff = find(isDisp1(y,:));
    imgInt1Relevant(y,x1p(y,ff),:) = img1(y,ff,:);
    dispInt1(y,x1p(y,ff)) = disp1(y,ff);

    ff = find(isDisp2(y,:));
    ff_rev = find(isDisp2(y,end:-1:1));
    imgInt2Relevant(y,x2p(y,ff),:) = img2(y,ff_rev(end:-1:1),:);
    dispInt2(y,x2p(y,ff)) = disp2(y,ff_rev(end:-1:1)); 
end 


%% Create one Intermediate Image (solving the doubble occlusion problem)

%Matrizen mit einer 1 wo sich l�cher in der neu berechnenten
%disparity map befinden
nanIdx1 = isnan(dispInt1);
nanIdx2 = isnan(dispInt2);

%berechnetete disparity map erh�lt den wert 0 an den stellen wo sie
%keinen Wert besitzt
dispInt1(nanIdx1) = 0;
dispInt2(nanIdx2) = 0;

%shows where no candidate pixel exists for either the first and the secound 
%image 
holes = zeros(sy,sx);
holes(nanIdx1 & nanIdx2) = nan; 

%zeigt wo der Disarity Wert1 gr��er(gleich) dem Dispartiy Wert2 ist und
%zugliech der Disparty Wert1 nicht NaN ist
dispGreater1 = (abs(dispInt1) >= abs(dispInt2)) & ~nanIdx1; 
%zeigt wo der Disarity Wert2 gr��er dem Dispartiy Wert1 ist, da alle Werte 
%die bei dispGreater1 = 0 sind folgloch einen gr��ernen
%dispInt2 Werte besitzen zugliech muss ein dispInt2 Wert an der jeweiligen 
%Stelle existieren
dispGreater2 = ~dispGreater1 & ~nanIdx2; 

%schreibe im Bild �berall eine 0 wo die disparity im anderen bild f�r das
%selbe Pixel gr��er ist 
imgInt1Relevant(repmat(~dispGreater1,[1,1,3])) = 0;
imgInt2Relevant(repmat(~dispGreater2,[1,1,3])) = 0;

%imgOut = (imgInt1Relevant+imgInt2Relevant)/255 + repmat(holes,[1 1 3]);
imgOut = (imgInt1Relevant+imgInt2Relevant) + repmat(holes,[1 1 3]);

%% Calculation of the refinement Map

%tic
[Fx,Fy] = sobel_xy(dispGreater1);
dispEdges1 = Fx | Fy ;
[Fx,Fy] = sobel_xy(dispGreater2);
dispEdges2 = Fx | Fy ;
refEdges = dispEdges1 | dispEdges2;
%toc
%figure;imshow(refEdges);

 % tic
 % %using morphological operations (pseudo edge detection):
 % s = [0 1 0; 1 1 1; 0 1 0];
 % dispEdges1 = imageDilate(dispGreater1, s) - dispGreater1;
 % dispEdges2 = imageDilate(dispGreater2, s) - dispGreater2;
 % refEdges = dispEdges1 | dispEdges2;
 % toc
 % figure;imshow(refEdges);

%delating the Result to to increase connectivity
s = [0 1 0; 1 1 1; 0 1 0];
refEdges = imageDilate(refEdges, s);
%refEdges = imageDilate(refEdges, s);


function [imgOut,holesEdges] = fillImage(imgIn, Nr, inc)
% F�llend der L�cher des Eingangsbildes mit dem Mittlernen
% Intensit�tswertwerte um ein Loch Pixel, dabei wird immer die Nachbaschaft
% der gr��e Nr betrachtet, ist in Nr ein Intensi�tswert enthalten (nur
% NaN-Werte), dann wird die Nachbaschaft Nr um inc vergr��ert 

% imgIn      = Eingangsbild mit L�chern  
% Nr         = Radius Nachbarschafft
% inc        = Vergr��erung von Nr wenn nichts gefunden

% imgOut     = Ausgangsbild in dem die L�cher gef�llt wurden 
% holesEdges = Bin�res Bild, welches die Kanten der L�cher enth�lt 

%% init Varibales 
imgOut = imgIn;
[sy sx , ~] = size(imgIn);
halfsize = floor([sy,sx]/2);
holes = isnan(imgIn(:,:,1));
disp('Detectieren der L�cher in der Virtuellen Ansicht')
%[~, anzLabels, PixelIdxList] = blobDetection(holes); 
[~, anzLabels, PixelIdxList] = fast_blobDetection(holes); 

validPixels = ~holes;

%% F�llen der L�cher mit dem Mittlernen Intensit�tswert der Pixel um ein Pixel des Lochs, NaN werte werden dabei ignoriert 
disp('F�llen der L�cher in der Virtuellen Ansicht')
for l = 1:anzLabels

    [m, n] = ind2sub([sy,sx],PixelIdxList{l});
    
    for k = 1 : length(m)
        while true
            i_patch = max(1, m(k)-Nr) : min(sy,m(k)+Nr);
            j_patch = max(1, n(k)-Nr) : min(sx,n(k)+Nr);
            valid = validPixels(i_patch,j_patch);
            if any(valid(:))
               break;
            elseif any(Nr >= halfsize)
                error('L�cher sind zu gross');
            else
                Nr = min([Nr+inc,halfsize]);
            end
        end
        
        imgPatch = imgIn(i_patch,j_patch,:);
        imgPatchReshape = reshape(imgPatch(:),[numel(imgPatch)/3 3]);
        imgPatchReshape(isnan(imgPatchReshape)) = [];
        imgPatchReshape = reshape(imgPatchReshape',[numel(imgPatchReshape)/3 3]);

        estIntensity = sum(imgPatchReshape)./length(imgPatchReshape);
        imgOut(m(k),n(k),:) = estIntensity;
        
    end
end

%% Berechne die Kanten der L�cher 
%s = [0 1 0; 1 1 1; 0 1 0];
%holesEdges = imageDilate(holes, s) - holes;

[Fx,Fy] = sobel_xy(holes);
holesEdges = Fx | Fy ;


%Um die Verbindung der einzlenen Randpixel zu erh�hen erfolgt noch abschli�end eine Dilatation
s = [0 1 0; 1 1 1; 0 1 0];
holesEdges = imageDilate(holesEdges, s);
holesEdges = imageDilate(holesEdges, s);



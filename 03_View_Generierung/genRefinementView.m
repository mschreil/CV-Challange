function refImgOut = genRefinementView(refEdges, intImg, d)
% Filtert das Eingangsbild mit  einem dxd Median Filter an allen Stellen
% wo refEdges eine 1 besitzt 
% Median Filter: Pxiel wert ist der Mittlere Werte des dxd Fensters 

% refEdges  = Bin�res Bild mit Werten von 0 und 1 
% intImg    = Eingangsbild auf das der Filter angewendet wird 
% d         = gr��e des Median-Filters d x d

% refImgOut = Gefilters Bild 

%% Terminalausgabe
disp('Filtern der Virtuellen Ansicht mit Median Filter')

%% Initialisierung der Variablen  

dh = floor(d/2);
[sy,sx,sz] = size(intImg);
edges = find(refEdges);
anzEdgeP = length(edges);
[m n] = ind2sub([sy,sx],edges); %Koordiantes of an edgePoint
refImgOut = intImg;

%% Filter Image with Median Filter

for k = 1:anzEdgeP
    i = max(1, m(k)-dh) : min(sy,m(k)+dh);
    j = max(1, n(k)-dh) : min(sx,n(k)+dh);
    
    % ausschneiden eines Patechs aus dem Bild
    imgPatch = intImg(i,j,:);
    anzPatch = numel(imgPatch(:,:,1));
    
    % Sortiere die Werte aus dem Patch der Reihe nach 
    %(RGB Werte jeweils in eigener Spalte)
    imgPatchSort = sort(reshape(imgPatch,[anzPatch 3]),1); %reshape: da imgPatch ein 3er tupel
    patchMiddle = floor(anzPatch/2);
    
    %wenn anz. Elemente im Patch gerade
    if patchMiddle*2 == anzPatch 
        refImgOut(m(k),n(k),:) = (sum(imgPatchSort(patchMiddle:patchMiddle+1,:))/2);
    %wenn anz. Elemente im Patch ungerade
    else
        refImgOut(m(k),n(k),:) = imgPatchSort(patchMiddle+1,:);
    end        
end





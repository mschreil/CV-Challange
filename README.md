Funktionsaufrufe: 

1: Rectifizeirung: 

	[rect_image1, rect_image2, steroImage] = rectification_fitted(image1, image2, 1);

		rect_image1 & rect_image2 = Rectifizierte und zugeschnittene Bilder 
		-> steroImage = rect_image1 und rect_image2 soll in GUI angezeigt werden <-
		   wenn möglich noch horizontale linien in das Bild einfügen: 
		   ähnlich zu: 
						figure; imshow(steroImage);
						hold on;
						for ii = 1:50:size(image1,1)
						line([1 size(image1,2)],[ii ii],'Color','red'); 
						end 
						hold off;


2: Disparity:

	[disparityMap_1, disparityMap_2] = disparityEstimation(rect_image1, rect_image2, mins, maxs, win_size, weight);
	--> Rückgegeben werden die beiden disparitymaps -> imshow muss der Richte Wertebereich übergeben werden da sonst die Anzeige nicht funktioniert 
	Beispiel zum Anzeigen der Disparity1: imshow(disparityMap_1,[mins,maxs]);

	-> Variablen die für jedes Bildpaar spezielle angepasst werden müssen 
	mins     = -260;
	maxs     = 70;
	win_size = 20;
	weight   = 100;

3:  View Synthesis: 

	1.Schritt:
		[imgOut1, refEdges] = genInitalView(p, image1, image2, disparityMap_1, disparityMap_2);
		-> ImgOut1 in GUI anzeigen <-

	2.Schritt:
		imgOut2 = genRefinementView(refEdges, imgOut1, 5);
		-> ImgOut2 in GUI anzeigen <-

	3.Schritt:
		[imgOut3,holesEdges] = fillImage(imgOut2, 15, 6);
		-> ImgOut3 in GUI anzeigen <-

	4.Schritt:
		imgOut4 = genRefinementView(holesEdges, imgOut3, 5)
		-> ImgOUt3 in GUI anzeigen <-

4:  Auflösung anpassen:
		imgOut = defaultresolution(imgOut4,ix,iy);
		Anpassen der Größe des berchneten Bildes an die größe der "Echten" Bilder
		-> imgOut = final Image in GUI anzeigen <-
		

FAST_Blob_Detection:
https://davidstutz.de/fast-multi-label-connected-components-in-opencv-and-matlab/


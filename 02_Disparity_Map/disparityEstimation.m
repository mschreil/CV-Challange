function [dsp1, dsp2] = disparityEstimation(i1,i2, mins, maxs, win_size, weight)
% Erstellen einer linken und einer Rechtendisparty Map 
% �ber SAD (Sum of Absolute differences) bezogen auf ein Graubild und dessen Gradienten
% die einzelenden Pixel werden dann noch mit einem Averaging Filter
% verrechneten - der Averaging Filter besitzt die Gr��e Window Size 

% i1       =   linkes Bild
% i2       =   rechtes Bild 
% mins     =   minimale Disparity, kann auch negativ sein !!
% maxs     =   maximale Disparity
% win_size =   gr��e des Fenster f�r den Averaging Filter
% weight   =   Gewichtung des Gradientenbildes gegen�ber dem Graubild 

% dsp1     =   disparity bezogen auf das erste Bild (i1)
% dsp2     =   disparity bezogen auf das zweite Bild (i2)
  

  %--�berpr�fe ob Bilder als Grauwertbilder �bergeben werden   
  if size(i1,3) > 1
      i1 = rgb_to_gray(i1); %VORSICHT Gray funktion muss NAN-Werte erhalten !!  
  end 
  
  if size(i2,3) > 1
      i2 = rgb_to_gray(i2); %VORSICHT Gray funktion muss NAN-Werte erhalten !!
  end 
 
  %-- Pre-Processing der Bilder
  i1 = gauss(i1,5,3);
  i2 = gauss(i2,5,3);
 
  
  %--Berechne Linke und Rechte Disparity Map  
  disp('Starte Berechnung der Disparity f�r Bild1');
  dsp1 = slide_images(i1,i2, mins, maxs, win_size, weight, true);
  disp('Starte Berechnung der Disparity f�r Bild2');
  dsp2 = slide_images(i2,i1, mins, maxs, win_size, weight, false);
  
  
  %-- Post-Processing der Bilder   
  dsp1 = medianfilter(dsp1);
  dsp2 = medianfilter(dsp2);
  
  
end

%% Hilfsfunktionen 

% -- slides images across each other to get disparity estimate

function [disparity] = slide_images(img1,img2,mins,maxs,win_size,weight, direction)
% Schiebt img2 �ber img1 um die Disparity Werte zu erhalten 
% wenn direction gleich TRUE dann image shift nach rechts
% wenn direction gleich FALSE dann image shift nach links

% figure;   

  print = false;

 
    if direction == true
        step = 1;
    end

    if direction == false 
        step = -1;
    end

    [dimy, dimx, ~] = size(img1);
    disparity = zeros(dimy,dimx);           %-- Initialisiere den Output
    mindiff = inf(dimy,dimx);    

    h = ones(win_size)/win_size.^2;         %-- averaging Filter

    [g1x, g1y] = gradient(double(img1));    %-- Berechnung des Gradienten 
    [g2x, g2y] = gradient(double(img2));


    for ii = (mins:maxs)*step              

        s  = shift_image(img2,ii);          %-- Schiebe Graubild und Gradientenbild
        sx = shift_image(g2x,ii);
        sy = shift_image(g2y,ii);

        %--CSAD  ist Cost von Sum of Absolute Differences
        %--CGRAD ist Cost von Gradient of Absolute Differences
        
        diffs = abs(img1-s);                %-- berechne CSAD und CGRAD
        gdiffx = abs(g1x-sx);
        gdiffy = abs(g1y-sy);
        gdiff = gdiffx+gdiffy;

        %CSAD  = imfilter(diffs,h);
        %CGRAD = imfilter(gdiff,h);
        CSAD = faltungs_filter(diffs, h);
        CGRAD = faltungs_filter(gdiff, h);
        
        d = CSAD+weight*CGRAD;              %-- berechne den Werte der Totalen Differenz
        %d = (1-weight)*CSAD+weight*CGRAD; 
        
        idx = find(d<mindiff);              %-- schreibe zugeh�rige Disparity 
        disparity(idx) = ii*step;           %-- in die entsprechenden Pixel
        mindiff(idx) = d(idx);

        if (~mod(ii,10) && print)
          display(ii);
          %imshow(img1/2+s/2);
        end

    end
end 
  

%-- Schieben eines Bildes
function I = shift_image(I,shift)
% schiebt das Bild I um die anzahlt(shift) pixel.
% wenn shift POSITIV dann wird das bild nach rechts verschoben (linksseit-
% iges Anf�gen von Null-Werten)
% wenn shift NEGATIV dann wird das bild nach links verschoben (rechtsseit-
% iges Anf�gen von Null-Werten)

  dimx = size(I,2);
  if(shift > 0)
    I(:,shift:dimx,:) = I(:,1:dimx-shift+1,:);
    I(:,1:shift-1,:) = 0;
    %I(:,1:shift-1,:) = NaN;
  else 
    if(shift<0)
      I(:,1:dimx+shift+1,:) = I(:,-shift:dimx,:);
      I(:,dimx+shift+1:dimx,:) = 0;
      %I(:,dimx+shift+1:dimx,:) = NaN;
    end  
  end
end 




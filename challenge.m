%% Computer Vision Challenge

close all
clc
clear

% Groupnumber:
group_number = 1;

% Groupmembers:
% members = {'Max Mustermann', 'Johannes Daten'};
members = {};

% Email-Adress (from Moodle!):
% mail = {'ga99abc@tum.de', 'daten.hannes@tum.de'};
mail = {};


%% Load images

img1 = imread('Images/L2.jpg'); %Linkes Bild
img2 = imread('Images/R2.jpg'); %Rechts Bild


%% Free Viewpoint Rendering

% start execution timer -> tic
tic;
outImage = free_viewpoint(img1, img2, 0.5);
toc;

% stop execution timer -> toc;
elapsed_time = toc


%% Display Output
% Display Virtual View
figure;
imshow(outImage);

